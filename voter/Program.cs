﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using VoteDLL;

namespace voter
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            ByteArray.Length = Net.MaxByte;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            FormLogin LogIn = new FormLogin();
            LogIn.ShowDialog();
            if (!LogIn.IsConnect)
                return;
            Files.Load();
            FormVoter open = new FormVoter(LogIn.client, false);
            open.ShowDialog();
        }
    }
}
