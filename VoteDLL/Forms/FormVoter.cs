﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace VoteDLL
{
    public partial class FormVoter : Form
    {
        public Client client;
        private List<ItemShow> items;
        private bool IsPanle;

        public FormVoter(Client client, bool IsPanle)
        {
            InitializeComponent();
            this.client = client;
            this.IsPanle = IsPanle;
        }
        private void AddPoint(string key, string[] ids)
        {
            ByteArray byteArray = new ByteArray();
            byteArray.Write('P');
            byteArray.Write(key);
            byteArray.Write(ids.Length);
            foreach (var id in ids)
            {
                byteArray.Write(id);
            }
            client.Write(byteArray.Values);
        }

        private void Read(byte[] array)
        {
            if (InvokeRequired)
            {
                Invoke(new EventByte(Read), array);
                return;
            }
            ByteArray command = new ByteArray(array);
            char hard = command.ReadChar();
            switch (hard)
            {
                case 'D':
                    {
                        items = new List<ItemShow>();
                        ItemsShow.Controls.Clear();
                        int length = command.ReadInt();
                        for (int i = 0; i < length; i++)
                        {
                            ItemData add = new ItemData(command);
                            ItemShow item = new ItemShow(add, IsPanle);
                            items.Add(item);
                            ItemsShow.Controls.Add(item.panle);
                        }
                        if (length == 0)
                        {
                            MessageBox.Show("אין פרוייקטים");
                            Close();
                        }
                    }
                    break;
                case 'P':
                    {
                        int length = command.ReadInt();
                        string[] ids = new string[length];
                        for (int i = 0; i < length; i++)
                        {
                            ids[i] = command.ReadString();
                        }
                        foreach (var item in items)
                        {
                            if (Array.IndexOf(ids, item.item.ID) != -1)
                            {
                                item.AddPoint(1);
                            }
                        }
                    }
                    break;
                case 'K':
                    {
                        bool Good = (command.ReadChar() == 'T');
                        if (!Good)
                        {
                            bool Exist = (command.ReadChar() == 'T');
                            if (Exist)
                                MessageBox.Show("מפתח זה כבר בחר");
                            else
                                MessageBox.Show("מפתח זה לא קיים");
                        }
                    }
                    break;
                case 'E':
                    {
                        Close();
                    }
                    break;
            }
        }

        private void FormVoter_Load(object sender, EventArgs e)
        {
            client.Read = Read;
            ByteArray byteArray = new ByteArray();
            byteArray.Write('A');
            client.Write(byteArray.Values);
            PanelTop.Visible = !IsPanle;
            Size = new Size(Width, Height + PanelTop.Height);
        }
        private void FormVoter_FormClosed(object sender, FormClosedEventArgs e)
        {
            ByteArray byteArray = new ByteArray();
            byteArray.Write('E');
            client.Write(byteArray.Values);
        }

        private void BoxKey_TextChanged(object sender, EventArgs e)
        {
            if (BoxKey.Lines.Length > 1)
                BoxKey.Text = BoxKey.Lines[0];
        }
        private void buttonSand_Click(object sender, EventArgs e)
        {
            if (ItemShow.Checks == 3)
            {
                string key = BoxKey.Text;
                string[] ids = new string[3];
                for (int i = 0; i < 3; i++)
                {
                    ids[i] = ItemShow.Selects[i].item.ID;
                }
                AddPoint(key, ids);
            }
            else
            {
                MessageBox.Show("לא נבחרו 3 פרויקטים");
            }
        }
    }
}
