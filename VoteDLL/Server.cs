﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace VoteDLL
{
    public class Server
    {
        private int port, maxByte;
        public List<Chat> ListChat;
        private TcpListener serverSocket;
        private TcpClient clientSocket;
        public Server(int Port, int MaxByte)
        {
            port = Port;
            maxByte = MaxByte;
        }

        public void Open()
        {
            serverSocket = new TcpListener(port);
            clientSocket = default(TcpClient);
            ListChat = new List<Chat>();

            serverSocket.Start();
            while (true)
            {
                clientSocket = serverSocket.AcceptTcpClient();

                byte[] bytesFrom = new byte[byte.MaxValue];
                string dataFromClient = null;

                NetworkStream networkStream = clientSocket.GetStream();
                networkStream.Read(bytesFrom, 0, byte.MaxValue);
                dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                Chat chat = new Chat();
                chat.Read = Read;
                chat.Start(clientSocket, networkStream, maxByte, this);
                ListChat.Add(chat);
            }
        }
        public void Stop()
        {
            foreach (var item in ListChat)
            {
                item.Stop();
            }
            clientSocket.Close();
            serverSocket.Stop();
        }

        public EventServer Read = null;
        public void Write(byte[] text)
        {
            foreach (var item in ListChat)
            {
                item.Write(text);
            }
        }
    }
    public class Chat
    {
        private int maxByte;
        private TcpClient clientSocket;
        private Thread thread;
        private NetworkStream serverStream;
        private Server TheServer;
        private bool exit = false;

        public void Start(TcpClient inClientSocket, NetworkStream serverStream, int MaxByte, Server theServer)
        {
            this.clientSocket = inClientSocket;
            this.serverStream = serverStream;
            maxByte = MaxByte;
            TheServer = theServer;
            exit = false;
            thread = new Thread(TheChat);
            thread.Start();
        }
        public void Stop()
        {
            exit = true;
            thread = null;
            TheServer.ListChat.Remove(this);
        }

        public EventServer Read;
        public void Write(byte[] array)
        {
            byte[] outStream = array;
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
        }

        private void TheChat()
        {
            int requestCount = 0;
            byte[] bytesFrom = new byte[maxByte];
            string dataFromClient = null;
            requestCount = 0;

            while (!exit)
            {
                try
                {
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    networkStream.Read(bytesFrom, 0, maxByte);
                    if (Read != null)
                        Read(this, bytesFrom);
                }
                catch (Exception ex)
                {
                   
                }
            }
        }
    }
}